<?php

require_once("Contact.php");

class ContactDao {

    private $connection;

    public function __construct() {
        $this->connection = $this->createConnection();
    }

    private function createConnection() {
        $user = 'root';
        $password = 'password';

        $connection = new PDO('mysql:dbname=contacts;host=127.0.0.1', $user, $password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $connection;
    }

    public function findAll() {
        $statement = $this->connection->prepare("select id, name, email, phone from contact");
        $statement->execute();

        $contacts = [];

        foreach ($statement as $row) {
            $contact = new Contact($row['name'], $row['email'], $row['phone'], intval($row['id']));
            $contacts[] = $contact;
        }

        return $contacts;
    }

    public function findById($id) {
        $statement = $this->connection->prepare("select id, name, email, phone from contact where id = :id");
        $statement->bindValue(":id", $id);
        $statement->execute();

        $contact = null;
        foreach ($statement as $row) {
            $contact = new Contact($row['name'], $row['email'], $row['phone'], $row['id']);
        }

        return $contact;
    }

    public function save($contact) {
        $statement = $this->connection->prepare("insert into contact (name, email, phone)
            values(:name, :email, :phone)");

        $statement->bindValue(':name', $contact->name);
        $statement->bindValue(':email', $contact->email);
        $statement->bindValue(':phone', $contact->phone);
        $statement->execute();

        $id = $this->connection->lastInsertId();
        $contact->id = $id;
        return $contact;
    }

    public function update($contact) {
        $statement = $this->connection->prepare("update contact
                    set name = :name, email = :email, phone = :phone
                    where id = :id");

        $statement->bindValue(":name", $contact->name);
        $statement->bindValue(":email", $contact->email);
        $statement->bindValue(":phone", $contact->phone);
        $statement->bindValue(":id", $contact->id);
        $statement->execute();
    }

    public function deleteById($id) {
        $statement = $this->connection->prepare("delete from contact where id = :id");
        $statement->bindValue(":id", $id);
        $statement->execute();
    }
}
