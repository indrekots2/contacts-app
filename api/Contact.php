<?php

class Contact {
    public $id;
    public $name;
    public $email;
    public $phone;

    public function __construct($name, $email, $phone, $id = null) {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->id = $id;
    }

    public function validate() {
        $errors = [];

        if (strlen($this->name) < 3) {
            $errors[] = "Name must be at least 3 characters long";
        }

        return $errors;
    }

    public static function fromAssocArray($array) {
        return new Contact($array['name'], $array['email'], $array['phone']);
    }
}
